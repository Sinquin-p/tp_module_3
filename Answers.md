# TP_Module_3

### gene name
gene name : LOC118277769

### Accession number
#### What is the accession number for this gene ?
JAKUHG020000042.1

#### What is the accession number of the protein encoded by this gene?
GHKU01027112.1

#### Download the sequences of this gene, transcript and protein encoded by this gene.

### Genomic context
#### In which genome assembly is this gene ?
Spodoptera frugiperda

### Bioproject
#### In which Bioproject is this gene ?
PRJNA849517

### Publication
#### In which publication this gene is associated ?
Burge S, Kelly E, Lonsdale D, Mutowo-Muellenet P, McAnulla C, Mitchell A, Sangrador-Vegas A, Yong SY, Mulder N, Hunter S. Manual GO annotation of predictive protein signatures: the InterPro approach to GO curation. Database (Oxford). 2012 Feb 1;2012:bar068. doi: 10.1093/database/bar068. PMID: 22301074; PMCID: PMC3270475.

Tang H, Finn RD, Thomas PD. TreeGrafter: phylogenetic tree-based annotation of proteins with Gene Ontology terms and other annotations. Bioinformatics. 2019 Feb 1;35(3):518-520. doi: 10.1093/bioinformatics/bty625. PMID: 30032202; PMCID: PMC6361231.

### GeneID
#### What is the name of this gene ?
odorant receptor 13a-like

### Chromosomal localization
#### What is the chromosomal localization of this gene ?
18
NC_036204.1
### BLASTN
#### Briefly describe the BLASTN results.
il fait partis de ca Select for downloading or viewing reports	
LR824532.2	Spodoptera littoralis genome assembly, chromosome: 1

### 2.3 Contig ID

Search one contig of Spodoptera littoralis.

Description of the selected contig
What is the ID of this selected contig ?
CADHRV010000001.1

Describe this contig.
File formats
What file formats are available for this selected contig ?
![Alt text] ~/Capture d’écran 2024-01-17 à 15.14.15.png

Gene intra-contig

Select a gene in this contig
Describe this contig.

What are the ID taxonomy of Spodoptera frugiperda and Spodoptera littoralis ?

### Use of EBI databases

How many protein sequences are present in the UniprotKB database ?
251 702 059

How many protein sequences are verified ?
570 420

How many protein sequences are not verified ?
251 131 639
